#ifndef _MY_INCLUDE_LOGME_HH_
#define _MY_INCLUDE_LOGME_HH_

#include <iostream>
#include <sstream>
#include <sys/time.h>

std::ostream& operator<<(std::ostream &_out, const struct ::timeval &_t);

#define LOG_ME(WHAT) \
do { \
    std::ostringstream __out__; \
    struct ::timeval __now__; \
    ::gettimeofday(&__now__, NULL); \
    __out__ << __now__ << " - " << WHAT << " {" << __FILE__ << ":" << __LINE__ << " " << __PRETTY_FUNCTION__ << "}\n"; \
    std::cout << __out__.str() << std::flush; \
} while (false)

#endif//_MY_INCLUDE_LOGME_HH_
