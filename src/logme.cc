#include "config.h"
#include "logme.hh"
#include <time.h>
#include <iomanip>

std::ostream& operator<<(std::ostream &_out, const struct ::timeval &_t)
{
    std::ostringstream o;
    struct ::tm t;
    ::localtime_r(&_t.tv_sec, &t);
    o << (1900 + t.tm_year) << "-"
      << std::setw(2) << std::setfill('0') << (t.tm_mon + 1) << "-"
      << std::setw(2) << std::setfill('0') << t.tm_mday << " "
      << std::setw(2) << std::setfill(' ') << t.tm_hour << ":"
      << std::setw(2) << std::setfill('0') << t.tm_min << ":"
      << std::setw(2) << std::setfill('0') << t.tm_sec << "."
      << std::setw(6) << std::setfill('0') << _t.tv_usec;
    return _out << o.str();
}
