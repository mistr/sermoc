#include "config.h"
#include "asyncio.hh"
#include "logme.hh"
#include "sysexception.hh"
#include <cerrno>
#include <cstdlib>
#include <iostream>

int main()
{
    try {
        async::io::instance();
        return EXIT_SUCCESS;
    }
    catch (const sys::error< EINVAL > &e) {
        LOG_ME("EINVAL: " << e.what() << " catched at");
        return EXIT_FAILURE;
    }
    catch (const std::exception &e) {
        LOG_ME("exception: " << e.what() << " catched at");
        return EXIT_FAILURE;
    }
}
