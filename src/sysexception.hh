#ifndef _MY_INCLUDE_SYSEXCEPTION_HH_
#define _MY_INCLUDE_SYSEXCEPTION_HH_

#include <sstream>
#include <stdexcept>

#define THROW_ME_NOW(EXCEPTION, ERRNO, WHAT) \
    do { \
        std::ostringstream __what__; \
        __what__ << WHAT << " throwed at {" << __FILE__ << ":" << __LINE__ << " " << __PRETTY_FUNCTION__ << "}"; \
        (EXCEPTION)(ERRNO, __what__.str()); \
    } while (false)

namespace sys
{

template < template <int=0> class e,
           int v00,     int v01 = 0, int v02 = 0, int v03 = 0,
           int v04 = 0, int v05 = 0, int v06 = 0, int v07 = 0,
           int v08 = 0, int v09 = 0, int v10 = 0, int v11 = 0,
           int v12 = 0, int v13 = 0, int v14 = 0, int v15 = 0 > struct throw_me_now;

template < template <int=0> class e, int v00 >
struct throw_me_now< e, v00 > {
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01 >
struct throw_me_now< e, v00, v01 > {
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02 >
struct throw_me_now< e, v00, v01, v02 > {
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03 >
struct throw_me_now< e, v00, v01, v02, v03 > {
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04 >
struct throw_me_now< e, v00, v01, v02, v03, v04 > {
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05 >
struct throw_me_now< e, v00, v01, v02, v03, v04, v05 > {
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05, int v06 >
struct throw_me_now< e, v00, v01, v02, v03, v04, v05, v06 > {
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        case v06:
            throw e< v06 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05, int v06, int v07 >
struct throw_me_now< e, v00, v01, v02, v03, v04, v05, v06, v07 > {
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        case v06:
            throw e< v06 >(_what);
        case v07:
            throw e< v07 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05, int v06, int v07,
                                   int v08 >
struct throw_me_now< e, v00, v01, v02, v03, v04, v05, v06, v07, v08 >
{
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        case v06:
            throw e< v06 >(_what);
        case v07:
            throw e< v07 >(_what);
        case v08:
            throw e< v08 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05, int v06, int v07,
                                   int v08, int v09 >
struct throw_me_now< e, v00, v01, v02, v03, v04, v05, v06, v07, v08, v09 >
{
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        case v06:
            throw e< v06 >(_what);
        case v07:
            throw e< v07 >(_what);
        case v08:
            throw e< v08 >(_what);
        case v09:
            throw e< v09 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05, int v06, int v07,
                                   int v08, int v09, int v10 >
struct throw_me_now< e, v00, v01, v02, v03, v04, v05, v06, v07, v08, v09, v10 >
{
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        case v06:
            throw e< v06 >(_what);
        case v07:
            throw e< v07 >(_what);
        case v08:
            throw e< v08 >(_what);
        case v09:
            throw e< v09 >(_what);
        case v10:
            throw e< v10 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05, int v06, int v07,
                                   int v08, int v09, int v10, int v11 >
struct throw_me_now< e, v00, v01, v02, v03, v04, v05, v06, v07, v08, v09, v10, v11 >
{
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        case v06:
            throw e< v06 >(_what);
        case v07:
            throw e< v07 >(_what);
        case v08:
            throw e< v08 >(_what);
        case v09:
            throw e< v09 >(_what);
        case v10:
            throw e< v10 >(_what);
        case v11:
            throw e< v11 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05, int v06, int v07,
                                   int v08, int v09, int v10, int v11, int v12 >
struct throw_me_now< e, v00, v01, v02, v03, v04, v05, v06, v07, v08, v09, v10, v11, v12 >
{
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        case v06:
            throw e< v06 >(_what);
        case v07:
            throw e< v07 >(_what);
        case v08:
            throw e< v08 >(_what);
        case v09:
            throw e< v09 >(_what);
        case v10:
            throw e< v10 >(_what);
        case v11:
            throw e< v11 >(_what);
        case v12:
            throw e< v12 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05, int v06, int v07,
                                   int v08, int v09, int v10, int v11, int v12, int v13 >
struct throw_me_now< e, v00, v01, v02, v03, v04, v05, v06, v07, v08, v09, v10, v11, v12, v13 >
{
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        case v06:
            throw e< v06 >(_what);
        case v07:
            throw e< v07 >(_what);
        case v08:
            throw e< v08 >(_what);
        case v09:
            throw e< v09 >(_what);
        case v10:
            throw e< v10 >(_what);
        case v11:
            throw e< v11 >(_what);
        case v12:
            throw e< v12 >(_what);
        case v13:
            throw e< v13 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05, int v06, int v07,
                                   int v08, int v09, int v10, int v11, int v12, int v13, int v14 >
struct throw_me_now< e, v00, v01, v02, v03, v04, v05, v06, v07, v08, v09, v10, v11, v12, v13, v14 >
{
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        case v06:
            throw e< v06 >(_what);
        case v07:
            throw e< v07 >(_what);
        case v08:
            throw e< v08 >(_what);
        case v09:
            throw e< v09 >(_what);
        case v10:
            throw e< v10 >(_what);
        case v11:
            throw e< v11 >(_what);
        case v12:
            throw e< v12 >(_what);
        case v13:
            throw e< v13 >(_what);
        case v14:
            throw e< v14 >(_what);
        default:
            throw d(_what);
        }
    }
};

template < template <int=0> class e, int v00, int v01, int v02, int v03, int v04, int v05, int v06, int v07,
                                   int v08, int v09, int v10, int v11, int v12, int v13, int v14, int v15 >
struct throw_me_now
{
    typedef typename e<>::default_exception d;
    void operator()(int _error_code, const std::string &_what) __attribute__ ((__noreturn__)) {
        switch (_error_code) {
        case v00:
            throw e< v00 >(_what);
        case v01:
            throw e< v01 >(_what);
        case v02:
            throw e< v02 >(_what);
        case v03:
            throw e< v03 >(_what);
        case v04:
            throw e< v04 >(_what);
        case v05:
            throw e< v05 >(_what);
        case v06:
            throw e< v06 >(_what);
        case v07:
            throw e< v07 >(_what);
        case v08:
            throw e< v08 >(_what);
        case v09:
            throw e< v09 >(_what);
        case v10:
            throw e< v10 >(_what);
        case v11:
            throw e< v11 >(_what);
        case v12:
            throw e< v12 >(_what);
        case v13:
            throw e< v13 >(_what);
        case v14:
            throw e< v14 >(_what);
        case v15:
            throw e< v15 >(_what);
        default:
            throw d(_what);
        }
    }
};

class exception:public std::runtime_error
{
public:
    exception(const std::string &_what): std::runtime_error(_what) { }
    exception(const exception &_src): std::runtime_error(_src) { }
    virtual ~exception() throw() { }
};

template < int error_code > class error:public exception
{
public:
    error(const std::string &_what): exception(_what) { }
    error(const error &_src): exception(_src) { }
    virtual ~error() throw() { }
    enum { ERRNO = error_code };
};

template < > class error< 0 >
{
public:
    typedef exception default_exception;
};

template < int v00,     int v01 = 0, int v02 = 0, int v03 = 0,
           int v04 = 0, int v05 = 0, int v06 = 0, int v07 = 0,
           int v08 = 0, int v09 = 0, int v10 = 0, int v11 = 0,
           int v12 = 0, int v13 = 0, int v14 = 0, int v15 = 0 > struct throw_exception_now
:   throw_me_now< error, v00, v01, v02, v03, v04, v05, v06, v07, v08, v09, v10, v11, v12, v13, v14, v15 > {
};

}//namespace sys

#endif//_MY_INCLUDE_SYSEXCEPTION_HH_
